const AccountFactory = artifacts.require("AccountFactory");
const Account = artifacts.require("Account");

// Global test values
const testHash =
  "0x7d5a99f603f231d53a4f39d1521f98d2e8bb279cf29bebfd0687dc98458e7f89";
const testHashFunction = 0x12;
const testSize = 0x20;

contract("AccountFactory", async accounts => {
  let deployedFactory;
  const user = accounts[1];

  beforeEach(async () => {
    // a fresh factory contract abstraction to work with
    deployedFactory = await AccountFactory.new();
  });

  it("creates an account contract", async () => {
    // verify the created account for a user is empty so we can later verify it
    // is written to
    let createdAccount = await deployedFactory.existingAccounts(user);
    assert.equal(createdAccount, 0);

    // create an account associated with the user
    await deployedFactory.createAccount({ from: user });
    createdAccount = await deployedFactory.existingAccounts(user);

    // if the account is created, it will have a non-zero address value for
    // the user
    assert.isOk(createdAccount);
  });

  it("only allows 1 account per user address", async () => {
    // try and create 2 accounts from the same user. a revert should be
    // thrown, which forces an exit from the try block
    try {
      await deployedFactory.createAccount({ from: user });
      await deployedFactory.createAccount({ from: user });
      assert.fail(); // we shouldn't reach here
    } catch (err) {
      // we got the expected error, assert it's truthy
      assert(err);
    }
  });

  it("saves the correct account address for a user and verify that user is the owner", async () => {
    /* Methodology
        1. Create an account from a user address
        2. Use the user address to retrieve the contract address from the factory
        3. Use the contract address to call the contract and obtain the owner
        4. If the owner is the user, we know the factory saved the right contract
           address
      */
    await deployedFactory.createAccount({ from: user });
    const accountAddress = await deployedFactory.existingAccounts(user);
    const accountInstance = await Account.at(accountAddress);
    const owner = await accountInstance.owner();
    assert.equal(owner, user);
  });

  it("completes a throughput test", async () => {
    // repeats a lot of what was done up above
    await deployedFactory.createAccount({ from: user });
    const accountAddress = await deployedFactory.existingAccounts(user);
    const accountInstance = await Account.at(accountAddress);
    await accountInstance.saveFile(testHash, testHashFunction, testSize, {
      from: user
    });
    const result = await accountInstance.hashes(0);
    assert.equal(result.hash, testHash);
    assert.equal(result.hash_function, testHashFunction);
    assert.equal(result.size, testSize);
  });
});

contract("Account", async accounts => {
  let deployedAccount;
  const user = accounts[1];

  beforeEach(async () => {
    // a fresh factory contract abstraction to work with
    deployedAccount = await Account.new(user);
  });

  it("saves and returns a hash", async () => {
    await deployedAccount.saveFile(testHash, testHashFunction, testSize, {
      from: user
    });
    const result = await deployedAccount.hashes(0);
    assert.equal(result.hash, testHash);
    assert.equal(result.hash_function, testHashFunction);
    assert.equal(result.size, testSize);
  });

  it("only lets the owner save a hash", async () => {
    try {
      await deployedAccount.saveFile(testHash, testHashFunction, testSize, {
        from: accounts[2] // not the user, so should revert
      });
      assert.fail(); // should not reach here
    } catch (err) {
      assert(err);
    }
  });
});
