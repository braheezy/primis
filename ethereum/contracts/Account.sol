pragma solidity >=0.5.6 <0.7.0;

contract AccountFactory {
    // All accounts this factory has made
    mapping(address => address) public existingAccounts;

    function createAccount() public {
        // Only 1 account per unique public key
        require(existingAccounts[msg.sender] == address(0));

        existingAccounts[msg.sender] =  address(new Account(msg.sender));
    }
}

contract Account {
    struct Multihash {
        bytes32 hash;           // (hex) the actual hash of the file
        uint8 hash_function;    // uniqiue hash function code
        uint8 size;             // (hex) size of hash in bytes, usually 32
    }
    // All hashes this contract has saved
    Multihash[] public hashes;
    // The address of the user who created this account
    address public owner;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    constructor (address _owner) public {
        owner = _owner;
    }

    function saveFile (bytes32 _hash, uint8 _hash_function, uint8 _size)
    public onlyOwner {
        hashes.push(Multihash(_hash, _hash_function, _size));
    }
}
